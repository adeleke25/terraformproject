
provider "aws" {
    region = "us-east-1"
  
}

variable "subnet_cidr_block" {
    description = "subnet cidr block"
  
}

variable "vpc_cidr_block" {
    description = "vpc cidr block"
  
}

variable avail_zone {}

resource "aws_vpc" "test-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name = "test_vpc-dev"
    }
  
}

 resource "aws_subnet" "test-subnet-1" {
    vpc_id = aws_vpc.test-vpc.id
     cidr_block = var.subnet_cidr_block
     availability_zone = var.avail_zone
     tags = {
        Name = "test_subnet_1"
    }
   
 }

 data "aws_vpc" "existing_vpc" {
    default = true
 }
    

     resource "aws_subnet" "test-subnet-2" {
    vpc_id = data.aws_vpc.existing_vpc.id
     cidr_block = "172.31.160.0/20"
     availability_zone = "us-east-1a"
     tags = {
        Name = "test_subnet_2_defualt"
    }
     

   
 }
